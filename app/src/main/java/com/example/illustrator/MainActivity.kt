package com.example.illustrator

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.MediaScannerConnection
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import top.defaults.colorpicker.ColorPickerPopup
import java.io.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var drawingView: DrawingBoardView

    companion object {
        private const val STORAGE_PERMISSION_CODE = 1
        private const val GALLERY = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawingView = findViewById<DrawingBoardView>(R.id.drawingView)

        //  drawingView.setBrushSize(5.0F)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == GALLERY){
                try{
                    if(data!!.data != null){
                        val imageBackground = findViewById<ImageView>(R.id.imgBackground)
                        imageBackground.setImageURI(data!!.data)
                    } else {
                        Toast.makeText(this, "Error parsing the image!", Toast.LENGTH_SHORT).show()
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }
        }
    }

    public fun onShowBrush(view: View) {
        val brushDialog = Dialog(this)
        brushDialog.setContentView(R.layout.dialog_brush)
        brushDialog.setTitle("Select Brush Size: ")

        brushDialog.show()

        val smallBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnSmallBrush)
        smallBrushBtn.setOnClickListener {
            drawingView.setBrushSize(4.0F)
            brushDialog.dismiss()
        }

        val mediumBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnMediumBrush)
        mediumBrushBtn.setOnClickListener {
            drawingView.setBrushSize(8.0F)
            brushDialog.dismiss()
        }

        val largeBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnLargeBrush)
        largeBrushBtn.setOnClickListener {
            drawingView.setBrushSize(12.0F)
            brushDialog.dismiss()
        }
    }

    public fun onShowColorPicker(view: View) {
        ColorPickerPopup.Builder(this)
            .initialColor(Color.RED) // Set initial color
            .enableBrightness(true) // Enable brightness slider or not
            .enableAlpha(true) // Enable alpha slider or not
            .okTitle("Choose")
            .cancelTitle("Cancel")
            .showIndicator(true)
            .showValue(true)
            .build()
            .show(view, object : ColorPickerPopup.ColorPickerObserver() {
                override fun onColorPicked(color: Int) {
                    drawingView.setColor(color)
                }

                fun onColor(color: Int, fromUser: Boolean) {}
            })
    }

    public fun openGallery(view: View) {
        if (isReadStorageAllowed()) {
            //open gallery
            val pickImageIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickImageIntent, GALLERY)
            //read the image
            //set it as background of the lowest layer
        } else {
            requestStoragePermission()
        }
    }

    public fun onUndo(view: View){
        drawingView.undo()
    }

    public  fun onRedo(view: View){
        drawingView.redo()
    }

    public fun onSave(view: View) {
        if(isWriteStorageAllowed()) {
            val bitmap = getBitmapFromView(findViewById(R.id.drawingViewContainer))
            SavedBitmapAsyncTask(bitmap).execute()
        }else {
            requestStoragePermission()
        }
    }

    private fun requestStoragePermission() {
        ActivityCompat.requestPermissions(this,
            arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            STORAGE_PERMISSION_CODE
        )
    }

    private fun isReadStorageAllowed():Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun isWriteStorageAllowed():Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun getBitmapFromView(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        val bgImage = view.background
        if(bgImage != null){
            bgImage.draw(canvas)
        }else{
            canvas.drawColor(Color.WHITE)
        }

        view.draw(canvas)

        return bitmap
    }

    private inner class SavedBitmapAsyncTask(val bitmap: Bitmap): AsyncTask<Any, Void, String>() {
        private lateinit var progressDialog: Dialog
        override fun onPreExecute() {
            super.onPreExecute()
            showProgressDialog()
        }

        override fun doInBackground(vararg params: Any?): String {
            var result = ""
            if (bitmap != null) {
                try {
                    val filename = "illustrator-" + (System.currentTimeMillis() / 1000) + ".png"
                    val bytes = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, bytes)

                    val basePath = externalCacheDir!!.absoluteFile.toString()
                    val filePath = basePath + File.separator + filename
                    val file = File(filePath)

                    val fos = FileOutputStream(file)
                    fos.write(bytes.toByteArray())
                    fos.close()

                    result = file.absolutePath
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(
                            applicationContext,
                            "Some issues while saving image!",
                            Toast.LENGTH_SHORT
                    ).show()
                }
            }
            return result
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            hideProgressDialog()
            if(! result!!.isEmpty()) {
                Toast.makeText(
                        applicationContext,
                        "Your image has been saved!",
                        Toast.LENGTH_SHORT
                ).show()

                MediaScannerConnection.scanFile(this@MainActivity, arrayOf(result), null) {
                    path, uri -> val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
                    shareIntent.type = "image/png"

                    startActivity(Intent.createChooser(shareIntent, "Share with: "))
                }
            }else{
                Toast.makeText(
                        applicationContext,
                        "Some issues while saving image!",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }

        private fun showProgressDialog() {
            progressDialog = Dialog(this@MainActivity)
            progressDialog.setContentView(R.layout.progress_dialog)
            progressDialog.show()
        }

        private fun hideProgressDialog() {
            if(progressDialog != null) {
                progressDialog.dismiss()
            }
        }
    }
}